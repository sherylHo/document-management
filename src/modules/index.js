import { combineReducers } from 'redux'
import documentReducer from './document/reducer'
import userReducer from './user/reducer'

export default combineReducers({
  document: documentReducer,
  user: userReducer,
})
