import {
  Fetch_All_Documents_Begin,
  Fetch_All_Documents_Success,
  Fetch_All_Documents_Failure,
  Delete_Documents_Begin,
  Delete_Documents_Failure,
  Delete_Documents_Success,
  Get_Documents_Detail_Begin,
  Get_Documents_Detail_Failure,
  Get_Documents_Detail_Success,
  Update_Document_Begin,
  Update_Document_Success,
  Update_Document_Failure,
  Create_Document_Begin,
  Create_Document_Success,
  Create_Document_Failure,
  Manage_Document_Form,
} from './types'

import http from '../httpCommon'

// mapping after receiving data from server
const mappingToDocument = (document) => ({
  name: document.Name,
  email: document.Email,
  phoneNumber: document.Phone_Number,
  address: document.Address,
  ktpNumber: document.KTP_Number,
  npwpNumber: document.NPWP_Number,
  passport: document.Passport_Number,
  id: document.id,
})

// mapping before send to server
const mappingFromDocument = (document) => ({
  Name: document.name,
  Email: document.email,
  Phone_Number: document.phoneNumber,
  Address: document.address,
  KTP_Number: document.ktpNumber,
  NPWP_Number: document.npwpNumber,
  Passport_Number: document.passport,
})

const mappingToListDocument = (documents) => {
  return documents && documents.map((document) => mappingToDocument(document))
}

export const fetchAllDocuments = () => (dispatch) => {
  dispatch({ type: Fetch_All_Documents_Begin })

  http
    .get('/documents')
    .then((res) => {
      dispatch({
        type: Fetch_All_Documents_Success,
        payload: {
          list: mappingToListDocument(res.data) || [],
        },
      })
    })
    .catch((err) => dispatch({ type: Fetch_All_Documents_Failure }))
}

export const deleteDocument = (id) => async (dispatch) => {
  dispatch({ type: Delete_Documents_Begin })

  const res = await http.delete(`/documents/${id}`)
  if (res.data) {
    dispatch({
      type: Delete_Documents_Success,
    })
  } else {
    dispatch({ type: Delete_Documents_Failure, payload: 'err' })
  }

  return res
}

export const getDocumentDetail = (id) => async (dispatch) => {
  dispatch({ type: Get_Documents_Detail_Begin })

  const res = await http.get(`/documents/${id}`)

  if (res.data) {
    dispatch({
      type: Get_Documents_Detail_Success,
      payload: {
        documentDetail: mappingToDocument(res.data) || {},
      },
    })
  } else {
    dispatch({ type: Get_Documents_Detail_Failure })
  }

  return res
}

export const updateDocument = (documentForm) => async (dispatch) => {
  console.log('here creator', documentForm)
  const params = mappingFromDocument(documentForm)

  dispatch({ type: Update_Document_Begin })

  const res = await http.put(`/documents/${documentForm.id}`, params)
  if (res && res.data.status === 'ok') {
    dispatch({
      type: Update_Document_Success,
    })
  } else {
    dispatch({ type: Update_Document_Failure })
  }

  return res
}

export const createDocument = (documentForm) => (dispatch) => {
  const params = {
    Name: documentForm.name,
    Email: documentForm.email,
    Phone_Number: documentForm.phoneNumber,
    Address: documentForm.address,
    KTP_Number: documentForm.ktpNumber,
    NPWP_Number: documentForm.npwpNumber,
    Passport_Number: documentForm.passport,
  }
  dispatch({ type: Create_Document_Begin })

  http
    .post(`/documents/`, params)
    .then((res) => {
      dispatch({
        type: Create_Document_Success,
      })
    })
    .catch((err) => dispatch({ type: Create_Document_Failure, payload: err }))
}

export const manageDocumentForm = (documentFormInfo) => (dispatch) => {
  dispatch({ type: Manage_Document_Form, payload: { ...documentFormInfo } })
}
