import {
  Fetch_All_Documents_Begin,
  Fetch_All_Documents_Success,
  Fetch_All_Documents_Failure,
  Delete_Documents_Begin,
  Delete_Documents_Failure,
  Delete_Documents_Success,
  Get_Documents_Detail_Begin,
  Get_Documents_Detail_Failure,
  Get_Documents_Detail_Success,
  Update_Document_Begin,
  Update_Document_Success,
  Update_Document_Failure,
  Create_Document_Begin,
  Create_Document_Success,
  Create_Document_Failure,
  Manage_Document_Form,
} from './types'

const initialState = {
  documentList: [],
  loadingList: false,
  loadingDelete: false,
  deleteErr: '',
  loadingDetail: false,
  loadingUpdate: false,
  updateErr: '',
  loadingCreate: false,
  createErr: '',
  documentDetail: {},
  documentFormInfo: {
    name: '',
    email: '',
    phoneNumber: '',
    address: '',
    ktpNumber: null,
    npwpNumber: null,
    passport: '',
    id: '',
  },
}

const documentReducer = (state = initialState, action) => {
  switch (action.type) {
    case Fetch_All_Documents_Begin:
      return {
        ...state,
        loadingList: true,
      }
    case Fetch_All_Documents_Success:
      return {
        ...state,
        documentList: action.payload.list,
        loadingList: false,
      }
    case Fetch_All_Documents_Failure:
      return {
        ...state,
        loadingList: false,
      }
    case Delete_Documents_Begin:
      return {
        ...state,
        loadingDelete: true,
      }
    case Delete_Documents_Success:
      return {
        ...state,
        loadingDelete: false,
      }
    case Delete_Documents_Failure:
      return {
        ...state,
        loadingDelete: false,
        deleteErr: action.payload,
      }
    case Get_Documents_Detail_Begin:
      return {
        ...state,
        loadingDetail: true,
      }
    case Get_Documents_Detail_Success:
      return {
        ...state,
        loadingDetail: false,
        documentDetail: action.payload.documentDetail,
      }
    case Get_Documents_Detail_Failure:
      return {
        ...state,
        loadingDetail: false,
      }
    case Update_Document_Begin:
      return {
        ...state,
        loadingUpdate: true,
      }
    case Update_Document_Success:
      return {
        ...state,
        loadingUpdate: false,
      }
    case Update_Document_Failure:
      return {
        ...state,
        loadingUpdate: false,
        updateErr: action.payload,
      }
    case Create_Document_Begin:
      return {
        ...state,
        loadingCreate: true,
      }
    case Create_Document_Success:
      return {
        ...state,
        loadingCreate: false,
      }
    case Create_Document_Failure:
      return {
        ...state,
        loadingCreate: false,
        createErr: action.payload,
      }
    case Manage_Document_Form:
      return {
        ...state,
        documentFormInfo: action.payload,
      }
    default:
      return state
  }
}

export default documentReducer
