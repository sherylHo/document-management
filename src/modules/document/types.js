export const Fetch_All_Documents_Begin = 'Fetch_All_Documents_Begin'
export const Fetch_All_Documents_Success = 'Fetch_All_Documents_Success'
export const Fetch_All_Documents_Failure = 'Fetch_All_Documents_Failure'

export const Delete_Documents_Begin = 'Delete_Documents_Begin'
export const Delete_Documents_Failure = 'Delete_Documents_Failure'
export const Delete_Documents_Success = 'Delete_Documents_Success'

export const Get_Documents_Detail_Begin = 'Get_Documents_Detail_Begin'
export const Get_Documents_Detail_Failure = 'Get_Documents_Detail_Failure'
export const Get_Documents_Detail_Success = 'Get_Documents_Detail_Success'

export const Update_Document_Begin = 'Update_Document_Begin'
export const Update_Document_Failure = 'Update_Document_Failure'
export const Update_Document_Success = 'Update_Document_Success'

export const Create_Document_Begin = 'Create_Document_Begin'
export const Create_Document_Failure = 'Create_Document_Failure'
export const Create_Document_Success = 'Create_Document_Success'

export const Manage_Document_Form = 'Manage_Document_Form'
