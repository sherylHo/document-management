import {
  Register_Begin,
  Register_Success,
  Register_Failure,
  Login_Begin,
  Login_Success,
  Login_Failure,
} from './types'

const initialState = {
  loadingRegister: false,
  loadingLogin: false,
  userInfo: {},
}

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case Register_Begin:
      return {
        ...state,
        loadingRegister: true,
      }
    case Register_Success:
      return {
        ...state,
        loadingRegister: false,
      }
    case Register_Failure:
      return {
        ...state,
        loadingRegister: false,
        registerErr: action.payload,
      }
    case Login_Begin:
      return {
        ...state,
        loadingLogin: true,
      }
    case Login_Success:
      return {
        ...state,
        loadingLogin: false,
        userInfo: action.payload,
      }
    case Login_Failure:
      return {
        ...state,
        loadingLogin: false,
      }
    default:
      return state
  }
}

export default userReducer
