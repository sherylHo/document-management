export const Register_Begin = 'Register_Begin'
export const Register_Success = 'Register_Success'
export const Register_Failure = 'Register_Failure'

export const Login_Begin = 'Login_Begin'
export const Login_Success = 'Login_Success'
export const Login_Failure = 'Login_Failure'
