import {
  Register_Begin,
  Register_Success,
  Register_Failure,
  Login_Begin,
  Login_Success,
  Login_Failure,
} from './types'

import http from '../httpCommon'

export const registerUser = (params) => async (dispatch) => {
  dispatch({ type: Register_Begin })

  const res = await http.post(`/register`, params)

  if (res && res.data.status === 'error') {
    dispatch({ type: Register_Failure })
  } else {
    dispatch({ type: Register_Success })
  }

  return res
}

export const loginUser = (params) => async (dispatch) => {
  dispatch({ type: Login_Begin })

  const res = await http.post(`/login`, params)

  if (res && res.data.status === 'error') {
    dispatch({ type: Login_Failure })
  } else {
    dispatch({ type: Login_Success, payload: res.data.user })
  }

  return res
}
