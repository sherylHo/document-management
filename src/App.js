import React from 'react'
import { Layout } from 'antd'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import 'antd/dist/antd.css'

import { routes } from './routes'

import Sider from './components/sider/Sider'
import './App.css'

const App = () => {
  return (
    <Router>
      <Layout style={{ minHeight: '100vh' }}>
        <Layout className="wrapper">
          <Switch>
            {routes.map((route, index) => (
              <Route
                key={index}
                path={route.path}
                exact={route.exact}
                component={route.main}
              />
            ))}
          </Switch>
        </Layout>
      </Layout>
    </Router>
  )
}

export default App
