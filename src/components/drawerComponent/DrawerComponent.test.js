import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'

import DrawerComponent from './DrawerComponent'

describe('DrawerComponent', () => {
  const props = {
    handleClose: jest.fn(),
  }
  it('should render correctly', () => {
    const wrapper = shallow(<DrawerComponent {...props} />)

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
