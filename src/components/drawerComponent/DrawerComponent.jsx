import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Drawer } from 'antd'

const DrawerComponent = ({ handleClose, visible, record }) => {
  const {
    name,
    email,
    phoneNumber,
    address,
    ktpNumber,
    npwpNumber,
    passport,
  } = record
  return (
    <Drawer
      title="Document detail"
      placement="right"
      closable={false}
      onClose={handleClose}
      visible={visible}
      width={400}
    >
      {record && (
        <Fragment>
          <p>
            <strong>Name: </strong>
            {name}
          </p>
          <p>
            <strong>Email: </strong> {email}
          </p>
          <p>
            <strong>Phone Number: </strong>
            {phoneNumber}
          </p>
          <p>
            <strong>Address: </strong>
            {address}
          </p>
          <p>
            <strong>KTP Number: </strong>
            {ktpNumber}
          </p>
          <p>
            <strong>NPWP Number: </strong>
            {npwpNumber}
          </p>
          <p>
            <strong>Passport: </strong>
            {passport}
          </p>
        </Fragment>
      )}
    </Drawer>
  )
}

DrawerComponent.propTypes = {
  handleClose: PropTypes.func.isRequired,
  visible: PropTypes.bool,
  record: PropTypes.object,
}

DrawerComponent.defaultProps = {
  visible: false,
  record: {},
}

export default DrawerComponent
