import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'

import ButtonGroup from './ButtonGroup'

describe('ButtonGroup', () => {
  it('should render correctly', () => {
    const wrapper = shallow(<ButtonGroup />)

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
