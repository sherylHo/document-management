import React from 'react'
import { useHistory } from 'react-router-dom'
import { Button } from 'antd'

import { useHandleCancel } from './ButtonGroup.utils'

import './ButtonGroup.css'

const ButtonGroup = () => {
  const history = useHistory()

  const handleCancel = useHandleCancel({ history })

  return (
    <div className="buttonGroup">
      <Button type="primary" htmlType="submit" className="reset-width">
        Submit
      </Button>
      <Button htmlType="button" onClick={handleCancel} className="reset-width">
        Cancel
      </Button>
    </div>
  )
}

export default ButtonGroup
