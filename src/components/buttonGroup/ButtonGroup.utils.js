import { useCallback } from 'react'

export const useHandleCancel = ({ history }) =>
  useCallback(() => {
    history.goBack()
  }, [history])
