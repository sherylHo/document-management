import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'

import Sider from './Sider'

describe('Sider Component', () => {
  it('should render correctly', () => {
    const wrapper = shallow(<Sider />)

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
