import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { Layout, Menu } from 'antd'

import { siders } from '../../routes'

import './Sider.css'

const Sider = () => {
  const [collapsed, setCollapsed] = useState(false)

  const onCollapse = (collapsed) => {
    setCollapsed(collapsed)
  }

  return (
    <Layout.Sider
      collapsible
      collapsed={collapsed}
      onCollapse={onCollapse}
      breakpoint="lg"
      collapsedWidth="0"
    >
      <div className="logo" />
      <Menu theme="dark" defaultSelectedKeys={1}>
        {siders.map((item) => (
          <Menu.Item key={item.key}>
            <Link to={item.to}> {item.text} </Link>
          </Menu.Item>
        ))}
      </Menu>
    </Layout.Sider>
  )
}

export default Sider
