import React from 'react'
import { mount } from 'enzyme'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import configureStore from 'redux-mock-store'

import LoginForm from './LoginForm'

const initialState = {}
const middleware = [thunk]
const mockStore = configureStore(middleware)
const store = mockStore(initialState)

describe('LoginForm container', () => {
  it('should render correctly', () => {
    const wrapper = mount(
      <Provider store={store}>
        <LoginForm />
      </Provider>,
    )

    expect(wrapper.find('h2').length).toBe(1)
    expect(wrapper.find('Input').length).toBe(2)
    expect(wrapper.find('Button').length).toBe(2)
  })
})
