import React from 'react'
import { Form, Row, Col, Input, Spin, Button } from 'antd'
import { useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom'

import { loginUser } from '../../modules/user/actionCreator'

import { useHandleSubmit } from './LoginForm.utils'

const { Item } = Form

const layout = {
  labelCol: { span: 8 },
}

/* eslint-disable no-template-curly-in-string */
const validateMessages = {
  required: '${label} is required',
  types: {
    email: '${label} is not a valid email!',
  },
}

const LoginForm = () => {
  const history = useHistory()
  const dispatch = useDispatch()
  const handleSubmit = useHandleSubmit({
    dispatch,
    loginUser,
    history,
  })

  return (
    <Spin spinning={false}>
      <Form
        {...layout}
        onFinish={handleSubmit}
        className="documentForm"
        validateMessages={validateMessages}
      >
        <Row>
          <Col offset={8}>
            <h2>Login Form</h2>
          </Col>
          <Col span={24}>
            <Item label="Username" name="username" rules={[{ required: true }]}>
              <Input />
            </Item>
          </Col>

          <Col span={24}>
            <Item
              name="password"
              label="Password"
              rules={[
                {
                  required: true,
                  message: 'Please input your password!',
                },
              ]}
              hasFeedback
            >
              <Input.Password />
            </Item>
          </Col>

          <Col span={16} offset={8}>
            <Button type="primary" htmlType="submit" className="reset-width">
              Submit
            </Button>
            Or
            <Button type="link" onClick={() => history.push('/')}>
              register now!
            </Button>
          </Col>
        </Row>
      </Form>
    </Spin>
  )
}

export default LoginForm
