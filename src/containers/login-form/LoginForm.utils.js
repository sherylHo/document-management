import { useCallback } from 'react'
import { message } from 'antd'

export const useHandleSubmit = ({ dispatch, loginUser, history }) =>
  useCallback(async (value) => {
    const res = await dispatch(loginUser(value))

    if (res.data.status === 'error') {
      message.error(res.data.error)
    } else {
      message.success('Successfully login')
      history.push({
        pathname: '/document',
        state: { userInfo: res.data.user },
      })
    }
  }, [])
