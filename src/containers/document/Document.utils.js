import { useCallback } from 'react'
import { Modal, message } from 'antd'

export const useViewDocument = ({ setVisible, setSelectedDocument }) =>
  useCallback(
    (record) => {
      setSelectedDocument(record)
      setVisible(true)
    },
    [setSelectedDocument, setVisible],
  )

export const useEditDocument = ({ history }) =>
  useCallback((record) => {
    history.push({
      pathname: `/document-form-1/${record.id}`,
    })
  }, [])

const { confirm } = Modal

export const useDeleteDocument = ({
  deleteDocument,
  dispatch,
  fetchAllDocuments,
  deleteErr,
}) =>
  useCallback(
    (record) => {
      confirm({
        title: 'Do you Want to delete this item?',
        async onOk() {
          const res = await dispatch(deleteDocument(record.id))

          if (!res.data) {
            message.error('Fail to delete document.')
          } else {
            dispatch(fetchAllDocuments())
            message.success('Successfully delete the item.')
          }
        },
      })
    },
    [deleteErr],
  )

export const useAddDocument = ({ history }) =>
  useCallback(() => {
    history.push({
      pathname: '/document-form-1',
      state: { from: 'document' },
    })
  }, [])
