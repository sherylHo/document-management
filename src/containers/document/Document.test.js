import React from 'react'
import { mount } from 'enzyme'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import configureStore from 'redux-mock-store'

import Document from './Document'

const initialState = {
  document: {
    documentList: [],
    loadingList: false,
    deleteErr: '',
  },
}
const middleware = [thunk]
const mockStore = configureStore(middleware)
const store = mockStore(initialState)

describe('Document container', () => {
  it('should render correctly', () => {
    const wrapper = mount(
      <Provider store={store}>
        <Document />
      </Provider>,
    )

    expect(wrapper.find('h1').length).toBe(1)
    expect(wrapper.find('DrawerComponent').length).toBe(1)
    expect(wrapper.find('Table').length).toBe(2)
  })
})
