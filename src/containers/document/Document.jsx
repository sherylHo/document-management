import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { Spin, Table, Button } from 'antd'
import { useDispatch, useSelector } from 'react-redux'

import {
  fetchAllDocuments,
  deleteDocument,
} from '../../modules/document/actionCreator'
import DrawerComponent from '../../components/drawerComponent/DrawerComponent'

import {
  useViewDocument,
  useEditDocument,
  useDeleteDocument,
  useAddDocument,
} from './Document.utils'
import './Document.css'

const Document = () => {
  const history = useHistory()
  const dispatch = useDispatch()
  const [visible, setVisible] = useState(false)
  const [selectedDocument, setSelectedDocument] = useState({})
  const onClose = () => {
    setVisible(false)
  }
  const documentInfo = useSelector((state) => state.document)
  const { documentList, loadingList, deleteErr } = documentInfo

  const handleViewDocument = useViewDocument({
    setVisible,
    setSelectedDocument,
  })
  const handleEditDocument = useEditDocument({ history })
  const handleDeleteDocument = useDeleteDocument({
    deleteDocument,
    dispatch,
    fetchAllDocuments,
    deleteErr,
  })
  const handleAddDocument = useAddDocument({ history })

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      width: '30%',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
      width: '20%',
    },
    {
      title: 'Phone number',
      dataIndex: 'phoneNumber',
      key: 'phoneNumber',
      width: '20%',
    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      render: (text, record) => (
        <div className="actions">
          <Button type="link" onClick={() => handleViewDocument(record)}>
            View
          </Button>
          <Button type="link" onClick={() => handleEditDocument(record)}>
            Edit
          </Button>
          <Button type="link" onClick={() => handleDeleteDocument(record)}>
            Delete
          </Button>
        </div>
      ),
    },
  ]

  useEffect(() => {
    dispatch(fetchAllDocuments())
  }, [])

  return (
    <div className="wrapper">
      <h1>Document List</h1>
      <Button type="primary" className="addBtn" onClick={handleAddDocument}>
        Add Document
      </Button>

      <Spin spinning={loadingList}>
        <Table columns={columns} dataSource={documentList} />
      </Spin>
      <DrawerComponent
        handleClose={onClose}
        visible={visible}
        record={selectedDocument}
      />
    </div>
  )
}

export default Document
