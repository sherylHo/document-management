import { renderHook } from '@testing-library/react-hooks'

import { useViewDocument, useEditDocument, useAddDocument } from './Document.utils'

describe('useViewDocument', () => {
  const setVisible = jest.fn()
  const setSelectedDocument = jest.fn()

  it('should call correctly', () => {
    const record = { name: 'test' }
    renderHook(() =>
      useViewDocument({ setVisible, setSelectedDocument })(record),
    )

    expect(setSelectedDocument).toBeCalledWith(record)
    expect(setVisible).toBeCalledWith(true)
  })
})

describe('useEditDocument', () => {
  const history = {
    push: jest.fn(),
  }

  it('should call correctly', () => {
    const record = { name: 'test', id: 1 }
    renderHook(() => useEditDocument({ history })(record))

    expect(history.push).toBeCalled()
  })
})

describe('useAddDocument', () => {
  const history = {
    push: jest.fn(),
  }

  it('should call correctly', () => {
    const record = { name: 'test', id: 1 }
    renderHook(() => useAddDocument({ history })(record))

    expect(history.push).toBeCalled()
  })
})
