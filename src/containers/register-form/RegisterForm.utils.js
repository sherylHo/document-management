import { useCallback } from 'react'
import { message } from 'antd'

export const useHandleSubmit = ({ history, dispatch, registerUser }) =>
  useCallback(async (value) => {
    const res = await dispatch(registerUser(value))

    if (res.data.status === 'error') {
      message.error(res.data.error)
    } else {
      message.success('Successfully register')
      history.push('/login')
    }
  }, [])
