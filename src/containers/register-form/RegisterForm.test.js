import React from 'react'
import { mount } from 'enzyme'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import configureStore from 'redux-mock-store'

import RegisterForm from './RegisterForm'

const initialState = {}
const middleware = [thunk]
const mockStore = configureStore(middleware)
const store = mockStore(initialState)

describe('RegisterForm container', () => {
  it('should render correctly', () => {
    const wrapper = mount(
      <Provider store={store}>
        <RegisterForm />
      </Provider>,
    )

    expect(wrapper.find('h2').length).toBe(1)
    expect(wrapper.find('Input').length).toBe(3)
    expect(wrapper.find('Button').length).toBe(1)
  })
})
