import React from 'react'
import { Form, Row, Col, Input, Spin, Button } from 'antd'
import { useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom'

import { registerUser } from '../../modules/user/actionCreator'
import { useHandleSubmit } from './RegisterForm.utils'

const { Item } = Form

const layout = {
  labelCol: { span: 8 },
}

/* eslint-disable no-template-curly-in-string */
const validateMessages = {
  required: '${label} is required',
  types: {
    email: '${label} is not a valid email!',
  },
}

const RegisterForm = () => {
  const history = useHistory()
  const dispatch = useDispatch()

  const handleSubmit = useHandleSubmit({
    dispatch,
    registerUser,
    history,
  })

  const checkMatchingPwd = ({ getFieldValue }) => ({
    validator(_, value) {
      if (!value || getFieldValue('password') === value) {
        return Promise.resolve()
      }
      return Promise.reject(
        new Error('The two passwords that you entered do not match!'),
      )
    },
  })

  return (
    <Spin spinning={false}>
      <Form
        {...layout}
        onFinish={handleSubmit}
        className="documentForm"
        validateMessages={validateMessages}
      >
        <Row>
          <Col offset={8}>
            <h2>Register Form</h2>
          </Col>
          <Col span={24}>
            <Item label="Username" name="username" rules={[{ required: true }]}>
              <Input />
            </Item>
          </Col>

          <Col span={24}>
            <Item
              name="password"
              label="Password"
              rules={[
                {
                  required: true,
                  message: 'Please input your password!',
                },
              ]}
              hasFeedback
            >
              <Input.Password />
            </Item>
          </Col>

          <Col span={24}>
            <Item
              name="confirmPwd"
              label="Confirm Password"
              dependencies={['password']}
              hasFeedback
              rules={[
                {
                  required: true,
                  message: 'Please confirm your password!',
                },
                checkMatchingPwd,
              ]}
            >
              <Input.Password />
            </Item>
          </Col>

          <Col span={8} offset={16}>
            <Button type="primary" htmlType="submit" className="reset-width">
              Submit
            </Button>
          </Col>
        </Row>
      </Form>
    </Spin>
  )
}

export default RegisterForm
