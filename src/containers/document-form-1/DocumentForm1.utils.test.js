import { renderHook } from '@testing-library/react-hooks'

import { useHandleSubmit } from './DocumentForm1.utils'

describe('useHandleSubmit', () => {
  const manageDocumentForm = jest.fn()
  const dispatch = jest.fn()
  const history = {
    push: jest.fn(),
  }
  const documentFormInfo = {}
  const documentID = 1

  it('should call correctly', () => {
    const record = { name: 'test', email: 'test@gmail.co' }
    renderHook(() =>
      useHandleSubmit({
        history,
        manageDocumentForm,
        dispatch,
        documentFormInfo,
        documentID,
      })(record),
    )

    expect(history.push).toBeCalled()
    expect(dispatch).toBeCalled()
  })
})
