import { useCallback, useEffect } from 'react'

export const useHandleSubmit = ({
  history,
  manageDocumentForm,
  dispatch,
  documentDetail,
  documentId,
  updateDocument
}) =>
  useCallback(
    (value) => {
      dispatch(
        // manageDocumentForm({
        //   ...documentDetail,
        //   name: value.name,
        //   email: value.email,
        //   id: documentId,
        // }),

        updateDocument({
          ...documentDetail,
          name: value.name,
          email: value.email,
          id: documentId,
        }),
      )
      history.push(`/document-form-2/${documentId}`)
    },
    [manageDocumentForm, documentDetail, documentId],
  )

export const useLoadDocumentDetail = ({
  documentId,
  dispatch,
  getDocumentDetail,
}) =>
  useEffect(() => {
    if (documentId) {
      dispatch(getDocumentDetail(documentId))
    }
  }, [documentId])
