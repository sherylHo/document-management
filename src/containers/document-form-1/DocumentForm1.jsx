import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Form, Row, Col, Input, Spin } from 'antd'
import { useHistory, useLocation, useParams } from 'react-router-dom'

import ButtonGroup from '../../components/buttonGroup/ButtonGroup'
import {
  manageDocumentForm,
  getDocumentDetail,
  updateDocument,
} from '../../modules/document/actionCreator'
import { useHandleSubmit, useLoadDocumentDetail } from './DocumentForm1.utils'

const { Item } = Form

const layout = {
  labelCol: { span: 6 },
}

/* eslint-disable no-template-curly-in-string */
const validateMessages = {
  required: '${label} is required',
  types: {
    email: '${label} is not a valid email!',
  },
}

const DocumentForm1 = (props) => {
  const [form] = Form.useForm()
  const history = useHistory()
  const { documentId } = useParams()
  const dispatch = useDispatch()
  const documentInfo = useSelector((state) => state.document)
  const { loadingDetail, documentDetail } = documentInfo
  const { name, email } = documentDetail

  const handleSubmit = useHandleSubmit({
    history,
    manageDocumentForm,
    dispatch,
    documentDetail,
    documentId,
    updateDocument,
  })

  useLoadDocumentDetail({
    documentId,
    dispatch,
    getDocumentDetail,
  })

  useEffect(() => {
    if (Object.keys(documentDetail)) {
      form.setFieldsValue({
        name,
        email,
      })
    }
  }, [documentDetail])

  useEffect(() => {
    if (!documentId) {
      dispatch(
        manageDocumentForm({
          name: '',
          email: '',
          phoneNumber: '',
          address: '',
          ktpNumber: null,
          npwpNumber: null,
          passport: null,
          id: '',
        }),
      )
    }
  }, [])

  return (
    <Spin spinning={loadingDetail}>
      <Form
        form={form}
        {...layout}
        onFinish={handleSubmit}
        className="documentForm"
        validateMessages={validateMessages}
      >
        <Row>
          <Col offset={8}>
            <h2>Document Form</h2>
          </Col>
          <Col span={24}>
            <Item label="Name" name="name" rules={[{ required: true }]}>
              <Input />
            </Item>
          </Col>

          <Col span={24}>
            <Item
              label="Email"
              name="email"
              rules={[{ type: 'email', required: true }]}
            >
              <Input />
            </Item>
          </Col>

          <Col span={16} offset={8}>
            <ButtonGroup />
          </Col>
        </Row>
      </Form>
    </Spin>
  )
}

export default DocumentForm1
