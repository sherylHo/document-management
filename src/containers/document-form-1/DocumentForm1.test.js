import React from 'react'
import { mount } from 'enzyme'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import configureStore from 'redux-mock-store'

import DocumentForm1 from './DocumentForm1'

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useLocation: () => ({
    pathname: '/',
  }),
}))

const initialState = {
  document: {
    documentFormInfo: { name: '', email: '' },
    loadingDetail: false,
    documentDetailErr: '',
  },
}
const middleware = [thunk]
const mockStore = configureStore(middleware)
const store = mockStore(initialState)

describe('DocumentForm1 container', () => {
  it('should render correctly', () => {
    const wrapper = mount(
      <Provider store={store}>
        <DocumentForm1 />
      </Provider>,
    )

    expect(wrapper.find('h2').length).toBe(1)
    expect(wrapper.find('Spin').length).toBe(1)
    expect(wrapper.find('Input').length).toBe(2)
  })
})
