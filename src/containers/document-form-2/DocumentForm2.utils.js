import { useCallback } from 'react'

export const useHandleSubmit = ({
  history,
  manageDocumentForm,
  dispatch,
  documentDetail,
  updateDocument,
}) =>
  useCallback(
    (value) => {
      dispatch(
        // manageDocumentForm({
        //   ...documentFormInfo,
        //   phoneNumber: value.phoneNumber,
        //   address: value.address,
        // }),
        updateDocument({
          ...documentDetail,
          phoneNumber: value.phoneNumber,
          address: value.address,
          id: documentDetail.id,
        }),
      )
      history.push(`/document-form-3/${documentDetail.id}`)
    },
    [manageDocumentForm, documentDetail],
  )
