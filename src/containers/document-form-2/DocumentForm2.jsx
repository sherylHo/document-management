import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Form, Row, Col, Input } from 'antd'
import { useHistory, useParams } from 'react-router-dom'

import ButtonGroup from '../../components/buttonGroup/ButtonGroup'
import {
  manageDocumentForm,
  getDocumentDetail,
  updateDocument,
} from '../../modules/document/actionCreator'

import { useLoadDocumentDetail } from '../document-form-1/DocumentForm1.utils'

import { useHandleSubmit } from './DocumentForm2.utils'

const { Item } = Form

const layout = {
  labelCol: { span: 6 },
}

const DocumentForm2 = () => {
  const [form] = Form.useForm()
  const history = useHistory()
  const { documentId } = useParams()
  const dispatch = useDispatch()
  const documentInfo = useSelector((state) => state.document)
  const {
    documentDetail,
    documentDetail: { phoneNumber, address },
  } = documentInfo

  const handleSubmit = useHandleSubmit({
    history,
    manageDocumentForm,
    dispatch,
    documentDetail,
    updateDocument,
  })

  useLoadDocumentDetail({ documentId, dispatch, getDocumentDetail })

  useEffect(() => {
    if (Object.keys(documentDetail)) {
      form.setFieldsValue({
        phoneNumber,
        address,
      })
    }
  }, [documentDetail])

  return (
    <Form
      {...layout}
      form={form}
      onFinish={handleSubmit}
      className="documentForm"
      preserve={true}
    >
      <Row>
        <Col offset={8}>
          <h2>Document Form</h2>
        </Col>
        <Col span={24}>
          <Item label="Phone number" name="phoneNumber">
            <Input />
          </Item>
        </Col>

        <Col span={24}>
          <Item label="Address" name="address">
            <Input />
          </Item>
        </Col>

        <Col span={16} offset={8}>
          <ButtonGroup />
        </Col>
      </Row>
    </Form>
  )
}

export default DocumentForm2
