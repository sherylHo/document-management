import { renderHook } from '@testing-library/react-hooks'

import { useHandleSubmit } from './DocumentForm2.utils'

describe('useHandleSubmit', () => {
  const manageDocumentForm = jest.fn()
  const dispatch = jest.fn()
  const history = {
    push: jest.fn(),
  }
  const documentFormInfo = {}
  const documentID = 1

  it('should call correctly', () => {
    const record = { phoneNumber: '123', address: 'test' }
    renderHook(() =>
      useHandleSubmit({
        history,
        manageDocumentForm,
        dispatch,
        documentFormInfo,
        documentID,
      })(record),
    )

    expect(history.push).toBeCalled()
    expect(dispatch).toBeCalled()
  })
})
