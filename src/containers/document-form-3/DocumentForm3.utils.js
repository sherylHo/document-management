import { useCallback } from 'react'
import { message } from 'antd'

export const useHandleSubmit = ({
  history,
  manageDocumentForm,
  dispatch,
  updateDocument,
  documentDetail,
  fetchAllDocuments,
  createDocument,
  updateErr,
  createErr,
}) =>
  useCallback(
    async (value) => {
      // Edit
      if (documentDetail.id) {
        const res = await dispatch(
          updateDocument({
            ...documentDetail,
            ktpNumber: value.ktpNumber,
            npwpNumber: value.npwpNumber,
            passport: value.passport,
            id: documentDetail.id,
          }),
        )

        if (res && res.data.status === 'ok') {
          message.success('Successfully update document.')
        } else {
          message.error('Fail to update document.')
        }
      }
      // else {
      //   // Create
      //   dispatch(
      //     createDocument({
      //       ...documentFormInfo,
      //       ktpNumber: value.ktpNumber,
      //       npwpNumber: value.npwpNumber,
      //       passport: value.passport,
      //     }),
      //   )

      //   if (createErr) {
      //     message.error('Fail to create document.')
      //   } else {
      //     message.success('Successfully create document.')
      //   }
      // }

      dispatch(
        manageDocumentForm({
          name: '',
          email: '',
          phoneNumber: '',
          address: '',
          ktpNumber: null,
          npwpNumber: null,
          passport: null,
          id: '',
        }),
      )

      dispatch(fetchAllDocuments())
      history.push('/document')
    },
    [manageDocumentForm, updateErr, createErr],
  )
