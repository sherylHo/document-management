import React from 'react'
import { mount } from 'enzyme'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import configureStore from 'redux-mock-store'

import DocumentForm3 from './DocumentForm3'

const initialState = {
  document: {
    documentFormInfo: { name: '', email: '' },
    updateErr: '',
    createErr: '',
  },
}
const middleware = [thunk]
const mockStore = configureStore(middleware)
const store = mockStore(initialState)

describe('DocumentForm3 container', () => {
  it('should render correctly', () => {
    const wrapper = mount(
      <Provider store={store}>
        <DocumentForm3 />
      </Provider>,
    )

    expect(wrapper.find('h2').length).toBe(1)
    expect(wrapper.find('Input').length).toBe(3)
    expect(wrapper.find('ButtonGroup').length).toBe(1)
  })
})
