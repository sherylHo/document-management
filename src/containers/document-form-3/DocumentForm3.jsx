import React, { useEffect } from 'react'
import { Form, Row, Col, Input } from 'antd'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory, useParams } from 'react-router-dom'

import ButtonGroup from '../../components/buttonGroup/ButtonGroup'
import {
  manageDocumentForm,
  updateDocument,
  fetchAllDocuments,
  createDocument,
  getDocumentDetail,
} from '../../modules/document/actionCreator'

import { useLoadDocumentDetail } from '../document-form-1/DocumentForm1.utils'
import { useHandleSubmit } from './DocumentForm3.utils'

const { Item } = Form

const layout = {
  labelCol: { span: 6 },
}

const DocumentForm3 = () => {
  const [form] = Form.useForm()
  const history = useHistory()
  const { documentId } = useParams()
  const dispatch = useDispatch()
  const documentInfo = useSelector((state) => state.document)
  const {
    documentDetail,
    documentDetail: { ktpNumber, npwpNumber, passport },
    updateErr,
    createErr,
  } = documentInfo

  const handleSubmit = useHandleSubmit({
    history,
    manageDocumentForm,
    dispatch,
    updateDocument,
    documentDetail,
    fetchAllDocuments,
    createDocument,
    updateErr,
    createErr,
  })

  useLoadDocumentDetail({ documentId, dispatch, getDocumentDetail })

  useEffect(() => {
    if (Object.keys(documentDetail)) {
      form.setFieldsValue({
        ktpNumber: ktpNumber,
        npwpNumber: npwpNumber,
        passport: passport,
      })
    }
  }, [documentDetail])

  return (
    <Form
      {...layout}
      form={form}
      onFinish={handleSubmit}
      className="documentForm"
      preserve={true}
    >
      <Row>
        <Col offset={8}>
          <h2>Document Form</h2>
        </Col>
        <Col span={24}>
          <Item label="KTP" name="ktpNumber">
            <Input />
          </Item>
        </Col>
        <Col span={24}>
          <Item label="NPWP" name="npwpNumber">
            <Input />
          </Item>
        </Col>
        <Col span={24}>
          <Item label="Passport" name="passport">
            <Input />
          </Item>
        </Col>

        <Col span={16} offset={8}>
          <ButtonGroup />
        </Col>
      </Row>
    </Form>
  )
}

export default DocumentForm3
