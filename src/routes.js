import Document from './containers/document/Document'
import DocumentForm1 from './containers/document-form-1/DocumentForm1'
import DocumentForm2 from './containers/document-form-2/DocumentForm2'
import DocumentForm3 from './containers/document-form-3/DocumentForm3'

import RegisterForm from './containers/register-form/RegisterForm'
import LoginForm from './containers/login-form/LoginForm'
// import PageNotFound from './containers/page-not-found/PageNotFound'

export const routes = [
  {
    path: '/register',
    exact: true,
    sidebar: () => <p>register</p>,
    main: RegisterForm,
  },
  {
    path: '/',
    exact: true,
    sidebar: () => <p>register</p>,
    main: RegisterForm,
  },
  {
    path: '/login',
    exact: true,
    sidebar: () => <p>login</p>,
    main: LoginForm,
  },
  {
    path: '/document',
    exact: true,
    sidebar: () => <p>Document</p>,
    main: Document,
  },
  {
    path: '/document-form-1/:documentId',
    exact: true,
    sidebar: () => <p>documentForm1</p>,
    main: DocumentForm1,
  },
  {
    path: '/document-form-1',
    exact: true,
    sidebar: () => <p>documentForm1</p>,
    main: DocumentForm1,
  },
  {
    path: '/document-form-2',
    exact: true,
    sidebar: () => <p>documentForm2</p>,
    main: DocumentForm2,
  },
  {
    path: '/document-form-2/:documentId',
    exact: true,
    sidebar: () => <p>documentForm2</p>,
    main: DocumentForm2,
  },
  {
    path: '/document-form-3/:documentId',
    exact: true,
    sidebar: () => <p>documentForm3</p>,
    main: DocumentForm3,
  },
]

export const siders = [
  {
    key: 1,
    to: '/document',
    text: 'Document',
  },
]
